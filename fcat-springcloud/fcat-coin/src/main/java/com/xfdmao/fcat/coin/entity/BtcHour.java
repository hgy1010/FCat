package com.xfdmao.fcat.coin.entity;

import java.util.Date;
import javax.persistence.*;

@Table(name = "btc_hour")
public class BtcHour implements Comparable<BtcHour>{
    @Id
    private Long id;

    private Long amount;

    private Double count;

    private Double open;

    private Double close;

    private Double low;

    private Double high;

    private Long vol;

    private Date time;

    private String symbol;

    private String period;

    /**
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return amount
     */
    public Long getAmount() {
        return amount;
    }

    /**
     * @param amount
     */
    public void setAmount(Long amount) {
        this.amount = amount;
    }

    /**
     * @return count
     */
    public Double getCount() {
        return count;
    }

    /**
     * @param count
     */
    public void setCount(Double count) {
        this.count = count;
    }

    /**
     * @return open
     */
    public Double getOpen() {
        return open;
    }

    /**
     * @param open
     */
    public void setOpen(Double open) {
        this.open = open;
    }

    /**
     * @return close
     */
    public Double getClose() {
        return close;
    }

    /**
     * @param close
     */
    public void setClose(Double close) {
        this.close = close;
    }

    /**
     * @return low
     */
    public Double getLow() {
        return low;
    }

    /**
     * @param low
     */
    public void setLow(Double low) {
        this.low = low;
    }

    /**
     * @return high
     */
    public Double getHigh() {
        return high;
    }

    /**
     * @param high
     */
    public void setHigh(Double high) {
        this.high = high;
    }

    /**
     * @return vol
     */
    public Long getVol() {
        return vol;
    }

    /**
     * @param vol
     */
    public void setVol(Long vol) {
        this.vol = vol;
    }

    /**
     * @return time
     */
    public Date getTime() {
        return time;
    }

    /**
     * @param time
     */
    public void setTime(Date time) {
        this.time = time;
    }

    /**
     * @return symbol
     */
    public String getSymbol() {
        return symbol;
    }

    /**
     * @param symbol
     */
    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    /**
     * @return period
     */
    public String getPeriod() {
        return period;
    }

    /**
     * @param period
     */
    public void setPeriod(String period) {
        this.period = period;
    }

    @Override
    public int compareTo(BtcHour btcHour) {
        return (int)(this.id-btcHour.id);
    }
}